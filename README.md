===========================
Atajos de teclado en Ubuntu
===========================

Aquí encontrarás el código fuente en formato plano SVG de todos los atajos de teclado que están definidos **por defecto** en una instalación de Ubuntu.

Estos atajos de teclado corresponden a la versión 18.10 Cosmic Cuttlefish. Sin embargo entiendo que serán los mismos para la versión 18.04.
